// #pragma once
#ifndef __FACE_RECOGNITION__
#define __FACE_RECOGNITION__

//using defines
#define USE_DLIB_CV
#define USE_CNN

#include <dlib/dnn.h>
#include <dlib/clustering.h>
#include <dlib/string.h>
#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/image_processing/render_face_detections.h>
#include <dlib/image_processing.h>
#include <dlib/image_io.h>
#include <iostream>


//numpy like C++ for numerical calculation
//#include "xtensor-blas/xlinalg.hpp"
//#include <vector>
//#include "xtensor/xarray.hpp"


//#include "xtensor-io/"
#if defined USE_OPENCV
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#endif

//#include "FaceModel.h"

using namespace std;
using namespace dlib;

#if defined USE_OPENCV
using namespace cv;
#endif

#define     xtLinalg            xt::linalg::norm
#define     CVMat               cv::Mat
#define     DLIB_IMAGE          dlib::matrix<dlib::rgb_pixel>




#if defined USE_CNN

//==================================================================

template <long num_filters, typename SUBNET> using con5d = con<num_filters, 5, 5, 2, 2, SUBNET>;
template <long num_filters, typename SUBNET> using con5 = con<num_filters, 5, 5, 1, 1, SUBNET>;
template <typename SUBNET> using downsampler = relu<affine<con5d<32, relu<affine<con5d<32, relu<affine<con5d<16, SUBNET>>>>>>>>>;
template <typename SUBNET> using rcon5 = relu<affine<con5<45, SUBNET>>>;
using net_type = loss_mmod<con<1, 9, 9, 1, 1, rcon5<rcon5<rcon5<downsampler<input_rgb_image_pyramid<pyramid_down<6>>>>>>>>;

//==================================================================

template <template <int, template<typename>class, int, typename> class block, int N, template<typename>class BN, typename SUBNET>
using residual = add_prev1<block<N, BN, 1, tag1<SUBNET>>>;

template <template <int, template<typename>class, int, typename> class block, int N, template<typename>class BN, typename SUBNET>
using residual_down = add_prev2<avg_pool<2, 2, 2, 2, skip1<tag2<block<N, BN, 2, tag1<SUBNET>>>>>>;

template <int N, template <typename> class BN, int stride, typename SUBNET>
using block = BN<con<N, 3, 3, 1, 1, relu<BN<con<N, 3, 3, stride, stride, SUBNET>>>>>;

template <int N, typename SUBNET> using ares = relu<residual<block, N, affine, SUBNET>>;
template <int N, typename SUBNET> using ares_down = relu<residual_down<block, N, affine, SUBNET>>;

template <typename SUBNET> using alevel0 = ares_down<256, SUBNET>;
template <typename SUBNET> using alevel1 = ares<256, ares<256, ares_down<256, SUBNET>>>;
template <typename SUBNET> using alevel2 = ares<128, ares<128, ares_down<128, SUBNET>>>;
template <typename SUBNET> using alevel3 = ares<64, ares<64, ares<64, ares_down<64, SUBNET>>>>;
template <typename SUBNET> using alevel4 = ares<32, ares<32, ares<32, SUBNET>>>;

using anet_type = loss_metric<fc_no_bias<
	128, avg_pool_everything<
	alevel0<
	alevel1<
	alevel2<
	alevel3<
	alevel4<
	max_pool<3, 3, 2, 2, relu<affine<con<32, 7, 7, 2, 2,
	input_rgb_image_sized<150>
	>>>>>>>>>>>>;

//==================================================================
#define     NETCNN             net_type
#define     ANETCNN            anet_type

#endif

#define shapePredictor        dlib::shape_predictor
#define DLIB_DESERIALIZE      dlib::deserialize
#define FACE_ASSERT           std::vector<full_object_detection>



#if defined USE_DLIB_CV

#define IMAGE_ASSERT            dlib::matrix<dlib::rgb_pixel>
#define VIMAGE_ASSERT			std::vector<dlib::matrix<dlib::rgb_pixel>>
#define FACE_DESC_ASSERT		std::vector<matrix<float, 0, 1>>

#elif defined USE_OPENCV

#define IMAGE_ASSERT            cv::Mat
#define typedef xt::xarray<double> facetensor;

#endif

// using namespace face;
// frontal_face_detector detector = dlib::get_frontal_face_detector();
// face::model faceModel;
// faceModel.poseposeModel68Point();
// face::model posePredictor68Point;
//dlib::full_object_detection

class FaceRecognition
{
private:
	// void init();

	//model::face faceModel;
	frontal_face_detector detector = dlib::get_frontal_face_detector();
	//    shape_predictor sp;
	char *m_fileName, *m_modelName;
	bool hasSetImage = false;
	dlib::shape_predictor sp;
	dlib::shape_predictor sp_small;
	NETCNN                net;
	ANETCNN               anet;
	std::vector<rectangle> faceDetected;
	std::vector<full_object_detection> rawfacelocation;
	IMAGE_ASSERT			m_image;
	VIMAGE_ASSERT			m_faces;
	std::vector<matrix<float, 0, 1>> face_descriptors;


public:
        FaceRecognition();
	//FaceRecognition(char * modelName);
	FaceRecognition(const char *imageFile);
	FaceRecognition(const char *imageFile, const char * modelName);
	~FaceRecognition();
        void setImage(const char* fileName);
	//    void setModelType(char* modelName);

	void set5PointFaceModel(const char* fileModel5PointName);
	void set68PointFaceModel(const char* fileModel68PointName);
	void setMMODFaceModel(const char* fileModelName);
	void setDNNFaceModel(const char* fileModelName);

	//    struct _css
	//    {
	//        dlib::rectangle _rect;
	//        // _rect.

	//    };
	//    dlib::rectangle rect2css(dlib::rectangle _rect);
	//    dlib::rectangle css2rect(dlib::rectangle _rect);

	//    IMAGE_ASSERT loadImage();
	//    IMAGE_ASSERT loadImage(char* imageFile);

	void loadImage();

	//    auto faceDetection();

	//    auto& raw_face_location();

	VIMAGE_ASSERT faceLandmarks();
	struct _css2rect
	{

	};

	//    // xtLinalg face_distance();
	//    facetensor face_distance();
	VIMAGE_ASSERT jitter_image(const matrix<rgb_pixel>& img);
        double getFaceDescriptor();
        double getFaceDescriptorJitter();


};
#endif
