//#pragma once

#include "facerecognition.h"

FaceRecognition::FaceRecognition()
{
	this->m_modelName = (char*)"hog";
#define USE_CNN
}

//FaceRecognition::FaceRecognition(char* modelName)
//{
//	this->m_modelName = modelName;
//	if (m_modelName == "hog")
//	{
//#define USE_HOG
//	}
//	else
//	{
//#define USE_CNN
//	}
//}

FaceRecognition::FaceRecognition(const char *imageFile)
{
	this->m_fileName = (char*)imageFile;
	this->m_modelName = (char*)"hog";
#define USE_HOG
}

FaceRecognition::FaceRecognition(const char *imageFile, const char *modelName)
{
	this->m_fileName = (char*)imageFile;
	this->m_modelName = (char*)modelName;
//	if (m_modelName == "hog")
//	{
//#define USE_HOG
//	}
//	else
//	{
//#define USE_CNN
//	}
}

FaceRecognition::~FaceRecognition()
{
	;
}

void FaceRecognition::setImage(const char *fileName)
{
	this->m_fileName = (char *)fileName;
	if (m_fileName  == nullptr)
	{
		std::cout << "fail to set image" << std::endl;
	}
	else
	{
	  std::cout << "success to set image" << fileName << std::endl;
	  hasSetImage = true;
	}
}

void FaceRecognition::set5PointFaceModel(const char *fileModel5PointName)
{
	DLIB_DESERIALIZE((char*)fileModel5PointName) >> this->sp_small;
}

void FaceRecognition::set68PointFaceModel(const char *fileModel68PointName)
{
	DLIB_DESERIALIZE((char*)fileModel68PointName) >> this->sp;
}

void FaceRecognition::setMMODFaceModel(const char *fileModelName)
{
	DLIB_DESERIALIZE((char*)fileModelName) >> this->net;
}

void FaceRecognition::setDNNFaceModel(const char *fileModelName)
{
	DLIB_DESERIALIZE((char*)fileModelName) >> anet;
}

//IMAGE_ASSERT FaceRecognition::loadImage()
//{
//  IMAGE_ASSERT temp_image_matrices;
//#if defined USE_DLIB_CV
//  dlib::load_image(temp_image, this->m_fileName);
//#elif defined USE_OPENCV
//  temp_image_matrices = cv::imread(this->m_fileName, CV_LOAD_IMAGE_COLOR);
//  if(!temp_image_matrices.data)
//  {
//          cout<< "cannot open path file image : " << imageFile <<endl;
//          return -1;
//  }
//#endif
//  this->face = temp_image_matrices;
//  return temp_image_matrices;
//}

//IMAGE_ASSERT FaceRecognition::loadImage(char* imageFile)
//{
//  IMAGE_ASSERT temp_image_matrices;
//  this->m_fileName = &imageFile;

//#if defined USE_DLIB_CV
//  dlib::load_image(temp_image, imageFile);
//  return temp_image_matrices;
//#elif defined USE_OPENCV
//  temp_image_matrices = cv::imread(imageFile, CV_LOAD_IMAGE_COLOR);
//  if(!temp_image_matrices.data)
//  {
//          cout<< "cannot open path file image : " << imageFile <<endl;
//          return -1;
//  }
//  return temp_image_matrices;
//#endif

//}

void FaceRecognition::loadImage()
{
#if defined USE_DLIB_CV
	dlib::load_image(this->m_image, this->m_fileName);
#elif defined USE_OPENCV
	this->m_image = cv::imread(this->m_fileName, CV_LOAD_IMAGE_COLOR);
	if (!temp_image_matrices.data)
	{
		cout << "cannot open path file image : " << imageFile << endl;
		return -1;
	}
#endif
}

//auto& raw_face_location()
//{
//	if(model == "cnn")
//	{
//	    auto resnetModel = this.faceModel.resnetModel();
//	    std::vector<matrix<float,0,1>> face_descriptors = resnetModel(img);
////	    return
//	}
//}

//auto faceDetection()
//{
//#if defined USE_CNN
////  NETCNN net;
//  dlib::matrix<dlib::rgb_pixel> image;
//  dlib::load_image(image, this->m_imageFile);
//  while(image.size() < 1800*1800)
//    dlib::pyramid_up(image);
////  auto detFace = this.faceModel.CNNFaceDetector(image);
//  std::vector<rectangle> detFace = this.net(image);
//  this.faceDetected = this.net(image);
//  return detFace;
//#elif defined USE_HOG
//  std::vector<matrix<rgb_pixel>> faces;
//  dlib::matrix<dlib::rgb_pixel> image;
//  dlib::load_image(image, this->m_imageFile);
//  dlib::pyramid_up(image);
//  std::vector<rectangle> detFace = this.detector(image);
//  this.faceDetected = this.detector(image);
//  return detFace;
//#endif
//}

//auto faceDetection()
//{
//  FaceRecognition::loadImage();
//  for (auto face : this.detector(this.m_image))
//  {
//      auto shape = this.sp(this.m_image, face);
//      dlib::matrix<dlib::rgb_pixel> face_chip;
//      dlib::extract_image_chip(this.m_image, get_face_chip_details(shape,150,0.25), face_chip);
//      this.m_faces.push_back(move(face_chip));
//  }
//  return this.m_faces;
//}


VIMAGE_ASSERT FaceRecognition::faceLandmarks()
{
	//FACE_ASSERT temp_faces;
	this->loadImage();
	for (auto face : detector(this->m_image))
	{
		auto shape = this->sp(this->m_image, face);
		dlib::matrix<dlib::rgb_pixel> face_chip;
		dlib::extract_image_chip(m_image, get_face_chip_details(shape, 150, 0.25), face_chip);
		this->m_faces.push_back(move(face_chip));
	}
	return this->m_faces;
}

VIMAGE_ASSERT FaceRecognition::jitter_image(const matrix<rgb_pixel>& img)
{
	thread_local dlib::rand rnd;
	std::vector<matrix<rgb_pixel>> crops;
	for (int i = 0; i < 100; ++i)
		crops.push_back(dlib::jitter_image(img, rnd));
	return crops;
}

double FaceRecognition::getFaceDescriptor()
{
	FACE_DESC_ASSERT face_descriptors = this->anet(this->m_faces);
	//std::vector<sample_pair> edges;
	return (double)dlib::length(face_descriptors[0]);
}

double FaceRecognition::getFaceDescriptorJitter()
{
        matrix<float, 0, 1> face_descriptor = mean(mat(this->anet(this->jitter_image(m_faces[0]))));
        return (double)dlib::length(face_descriptors[0]);
}






