#include "FaceModel.h"
#include <iostream>

//shapePredictor model::face::poseModel68Point()
//{
//    shapePredictor temp_model68Point;
//    // ifstream path("../models/shape_predictor_68_face_landmarks.dat");
//    DLIB_DESERIALIZE("../models/shape_predictor_68_face_landmarks.dat")>>temp_model68Point;
//    return temp_model68Point;
//}

//shapePredictor model::face::poseModel5Point()
//{
//    shapePredictor temp_model5Point;
//    // ifstream path("../models/shape_predictor_5_face_landmarks.dat");
//    DLIB_DESERIALIZE("../models/shape_predictor_5_face_landmarks.dat")>>temp_model5Point;
//    // path>>temp_model5Point;
//    return temp_model5Point;
//}

//anetType model::face::resnetModel()
//{
//    anetType temp_resnetModel;
//    DLIB_DESERIALIZE("../models/dlib_face_recognition_resnet_model_v1.dat")>>temp_resnetModel;
//    return temp_resnetModel;
//}

//netType model::face::CNNFaceDetector()
//{
//    netType temp_cnnFaceDetector;
//    DLIB_DESERIALIZE("../models/mmod_human_face_detector.dat")>>temp_cnnFaceDetector;
//    return temp_cnnFaceDetector;
//}

shapePredictor model::face::poseModel68Point()
{
	shapePredictor temp_model68Point;
	DLIB_DESERIALIZE("../models/shape_predictor_68_face_landmarks.dat") >> temp_model68Point;
	return temp_model68Point;
}

shapePredictor model::face::poseModel5Point()
{
	shapePredictor temp_model5Point;
	DLIB_DESERIALIZE("../models/shape_predictor_5_face_landmarks.dat") >> temp_model5Point;
	return temp_model5Point;
}

anetType model::face::resnetModel()
{
	anetType temp_resnetModel;
	DLIB_DESERIALIZE("../models/dlib_face_recognition_resnet_model_v1.dat") >> temp_resnetModel;
	return temp_resnetModel;
}

netType model::face::CNNFaceDetector()
{
	netType temp_cnnFaceDetector;
	DLIB_DESERIALIZE("../models/mmod_human_face_detector.dat") >> temp_cnnFaceDetector;
	return temp_cnnFaceDetector;
}

shapePredictor model::face::getModel(char* modelPath)
{
	shapePredictor temp_modelPath;
	DLIB_DESERIALIZE(modelPath) >> temp_modelPath;
	return temp_modelPath;
}

netType model::face::getNetModel(char* modelPath)
{
	shapePredictor temp_modelPath;
	DLIB_DESERIALIZE(modelPath) >> temp_modelPath;
	return temp_modelPath;
}

anetType model::face::getAnetModel(char* modelPath)
{
	netType temp_modelPath;
	DLIB_DESERIALIZE(modelPath) >> temp_modelPath;
	return temp_modelPath;
}


