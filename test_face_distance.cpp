#define USE_CNN
#define USE_68POINTS

#include "FaceRecognition.h"
#include <chrono>

int main()
{
	FaceRecognition ref("ref.jpg");
	FaceRecognition test("test.jpg");

	ref.set68PointFaceModel("models/shape_predictor_68_face_landmarks.dat");
	ref.set5PointFaceModel("models/shape_predictor_5_face_landmarks.dat");
	ref.setMMODFaceModel("models/mmod_human_face_detector.dat");
	ref.setDNNFaceModel("models/dlib_face_recognition_resnet_model_v1.dat");

	test.set68PointFaceModel("models/shape_predictor_68_face_landmarks.dat");
	test.set5PointFaceModel("models/shape_predictor_5_face_landmarks.dat");
	test.setMMODFaceModel("models/mmod_human_face_detector.dat");
	test.setDNNFaceModel("models/dlib_face_recognition_resnet_model_v1.dat");

	double testScore = test.getFaceDescriptorJitter();
	double refScore = ref.getFaceDescriptorJitter();

	double diffScore = abs(testScore - refScore);
}