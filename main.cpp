//#include <iostream>


//using namespace std;

//int main()
//{
//  cout << "Hello World!" << endl;
//  return 0;
//}


#define USE_CNN
#define USE_68POINTS

#include "facerecognition.h"
#include <chrono>

int main()
{
	FaceRecognition ref;
	FaceRecognition test;

	ref.setImage("/media/marlonamadeus/DATA/Projects/FRL/Data/Test/ref.jpg");
	test.setImage("/media/marlonamadeus/DATA/Projects/FRL/Data/Test/test.jpg");

	ref.set68PointFaceModel("/media/marlonamadeus/DATA/Projects/FRL/Data/Test/models/shape_predictor_68_face_landmarks.dat");
	ref.set5PointFaceModel("/media/marlonamadeus/DATA/Projects/FRL/Data/Test/models/shape_predictor_5_face_landmarks.dat");
	ref.setMMODFaceModel("/media/marlonamadeus/DATA/Projects/FRL/Data/Test/models/mmod_human_face_detector.dat");
	ref.setDNNFaceModel("/media/marlonamadeus/DATA/Projects/FRL/Data/Test/models/dlib_face_recognition_resnet_model_v1.dat");

	test.set68PointFaceModel("/media/marlonamadeus/DATA/Projects/FRL/Data/Test/models/shape_predictor_68_face_landmarks.dat");
	test.set5PointFaceModel("/media/marlonamadeus/DATA/Projects/FRL/Data/Test/models/shape_predictor_5_face_landmarks.dat");
	test.setMMODFaceModel("/media/marlonamadeus/DATA/Projects/FRL/Data/Test/models/mmod_human_face_detector.dat");
	test.setDNNFaceModel("/media/marlonamadeus/DATA/Projects/FRL/Data/Test/models/dlib_face_recognition_resnet_model_v1.dat");

	double testScore = test.getFaceDescriptor();
	double refScore = ref.getFaceDescriptor();

	double diffScore = abs(testScore - refScore);
}
